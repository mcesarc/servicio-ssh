#include "launcher.h"
//Tomado de: /from: https://askubuntu.com/questions/288494/run-system-commands-from-qml-app/314095#314095
//Autores: /Authors: int_ua   Y/AND  Jason Conti
Launcher::Launcher(QObject *parent) :
    QObject(parent),
    m_process(new QProcess(this))
{
}

QString Launcher::launch(const QString &program)
{
    m_process->start(program);
    m_process->waitForFinished(-1);
    QByteArray bytes = m_process->readAllStandardOutput();
    QString output = QString::fromLocal8Bit(bytes);
    return output;
}


Launcher::~Launcher() {

}
