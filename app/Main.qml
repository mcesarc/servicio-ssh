import QtQuick 2.0
import Ubuntu.Components 1.1
import Ubuntu.Components.Popups 0.1
import Servicio_Ssh 1.0
import "motor.js" as JS
/*!
    \brief MainView with Tabs element.
           First Tab has a single Label and
           second Tab has a single ToolbarAction.
*/

MainView {
    // objectName for functional testing purposes (autopilot-qt5)
    objectName: "mainView"

    // Note! applicationName needs to match the "name" field of the click manifest
    applicationName: "mlogicial.servicio-ssh.nexcor"

    /*
     This property enables the application to change orientation
     when the device is rotated. The default is false.
    */
    //automaticOrientation: true

    // Removes the old toolbar and enables new features of the new header.
    useDeprecatedToolbar: false

    width: units.gu(100)
    height: units.gu(75)

    Launcher {
        id: lanzador
    }

    Page {
        title: i18n.tr("servicio-ssh")
        id:pag;

        property string comando_ssh: "/etc/init.d/ssh ";
        property string comando_pid: "pidof sshd";
        property string comando_kill: "kill -9 ";
        property string comando_ip_lan: "ifconfig | grep 'inet addr' | grep -v '127.0.0.1' | grep  'Bcast' | cut  -d: -f2  | cut -d' '  -f1";
        property string clave_f:"";
        property int ancho_etiquetas: 20;
        property int ancho_etiquetas_dos: 4;

        Column {
            spacing: units.gu(1)
            anchors {
                margins: units.gu(2)
                fill: parent
            }

            Row{
                spacing: units.gu(5)

                Label {
                    id: ssh_t
                    text: i18n.tr("Servicio ssh:")
                    fontSize: "large"
                    font.bold: true
                    width: units.gu(pag.ancho_etiquetas)+units.gu(3)
                }

                Switch  {
                    function toggle() {
                        var resultado="";

                        if (mySwitch.checked== true){
                            console.log('Se intenta desactivar');
                            resultado=JS.ejecutar(pag.comando_ssh+" stop",true);
                            console.log(resultado);
                            if(resultado.search("stop")>0){
                                mySwitch.checked = false;
                                clave_e=false;
                                console.log('desactivado');
                                resultado=JS.ejecutar(pag.comando_pid,false);
                                if(resultado.length>0){
                                    console.log('Hay procesos activos: '+resultado);
                                    resultado=JS.ejecutar(pag.comando_kill+"$("+pag.comando_pid+")",true);
                                }
                            }else{
                                clave_e=true;
                            }

                        }else{
                            console.log('Se intenta activar');
                            resultado=JS.ejecutar(pag.comando_ssh+" start",true);
                            console.log(resultado);
                            if(resultado.search("start")>0){
                                mySwitch.checked = true;
                                clave_e=false;
                                console.log('activado');
                            }else{
                                clave_e=true;
                            }
                        }
                        if(clave_e===true)PopupUtils.open(dialog);
                    }
                    function comprobar() {
                        if(JS.hayQuePreguntarClave()){
                            PopupUtils.open(dialog);
                        }
                        else mySwitch.toggle();
                    }

                    id: mySwitch
                    // objectName: "interruptor"
                    property bool clave_e: false
                    checked: false
                    MouseArea { anchors.fill: parent; onClicked: mySwitch.comprobar() }
                }

            }//row
            Row{
                spacing: units.gu(5)
                //anchors.horizontalCenter: parent.baselineOffset
                Label {
                    id: ip_lan
                    //anchors.topMargin: 300
                    text: i18n.tr("Dirección IP LAN:")
                    //anchors.horizontalCenter: parent.horizontalCenter
                    fontSize: "medium"
                    font.bold: true
                    width: units.gu(pag.ancho_etiquetas)
                }
                Label {
                    id: iplan
                    text: i18n.tr("Sin determinar")
                    fontSize: "medium"
                    width: units.gu(pag.ancho_etiquetas_dos)
                }
            }

            Text {

                text: i18n.tr("El servicio ssh está activo")
                visible: mySwitch.checked == true
                font.pixelSize: FontUtils.sizeToPixels("medium")
            }

        }//column


    }//page

    Component.onCompleted: {
        JS.ssh_status();
        JS.dameIpLAN();

    }

    Component {
        id: dialog

        Dialog {
            id: dialogue
            title: i18n.tr("Se requiere autenticación")
            text: i18n.tr("Introduzca su clave")
            TextField {
                id: clave
                echoMode: TextInput.Password
                placeholderText: "Contraseña"

            }
            Button {
                text: i18n.tr("Aceptar")
                color: UbuntuColors.green
                onClicked: {
                    pag.clave_f=clave.text;
                    mySwitch.toggle()
                    PopupUtils.close(dialogue)
                }
            }
            Button {
                text: i18n.tr("Cancelar")
                color: UbuntuColors.orange
                onClicked: {
                    clave.text=""
                    pag.clave_f=""
                    mySwitch.clave_e=false
                    PopupUtils.close(dialogue);//Qt.quit()
                }
            }
            Text {

                text: i18n.tr("La clave era incorrecta")
                visible: mySwitch.clave_e
                color: "red"
                font.pixelSize: FontUtils.sizeToPixels("medium")
            }

        }//fin dialog

    }//fin component
}//fin

