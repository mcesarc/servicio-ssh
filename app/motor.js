function ejecutar(comando, esSuperU) {
    var prefijo=(esSuperU)?"echo '"+pag.clave_f+"' | sudo -S ":"";
    var resultado=String(lanzador.launch("bash -c \""+prefijo+comando+"\""));
    return resultado;
}
function ssh_status(){
    var modificador=" status";
    var resultado = ejecutar(pag.comando_ssh+modificador, false);
    console.log("en ssh_status");
    console.log(resultado);
    if(resultado.search("start")>0)
        mySwitch.checked=true;
    else{
        mySwitch.checked=false;
    }
    console.log("saliendo de ssh_status");

}
function dameIpLAN(){
    try{
        console.log("en iplan");
        var resultado=ejecutar(pag.comando_ip_lan, false);
        iplan.text=resultado;
        console.log(resultado);
    }catch(e){
        iplan.text="No pudo determinarse";
    }
    console.log("saliendo de iplan");
}
function hayQuePreguntarClave(){
    return pag.clave_f==="";
}
