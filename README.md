#servicio-ssh

#LÉEME 

## SERVICIO SSH   ##

Aplicación móvil para Ubuntu Touch que indica el estado del servicio ssh y permite activarlo o desactivarlo.

Características:

-Indicador de estado del servicio ssh
-Permite activa o desactivar el servicio ssh
-Indica la dirección IP (LAN) del móvil 

La activación del servicio ssh en el móvil permite conexiones ssh y sftp al móvil desde cualquier equipo de nuestra red interna (LAN).

NOTA*
Para poder acceder al servicio ssh la aplicación no está confinada. 


# README 

##  SSH SERVICE  ##

Ubuntu Touch mobile application for indicating the status of ssh service and allows ON or OFF.

features:

-Status Indicator ssh service
-Allows active or deactivate the ssh service
-Indicates IP address (LAN) mobile

Activation of ssh service on mobile allows ssh and sftp connections to the mobile from any computer on your LAN.

NOTE*
To access the service ssh, application is unconfined.